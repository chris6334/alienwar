part of alien_war;

abstract class Game {
  Renderer renderer;
  AudioManager audioManager;
  Controls controls;
  Earth earth;
  Set<Entity> _entities;
  Set<Entity> _needsAdded;
  Set<Entity> _needsRemoved;
  Entity player;
  bool loaded = false;

  Game(this.player) {
    _entities = new Set<Entity>();
    _needsAdded = new Set<Entity>();
    _needsRemoved = new Set<Entity>();

    renderer = new Renderer(this, _start);

    audioManager = new AudioManager();
    AudioClip musicClip = audioManager.makeClip('music', 'audio/telemetrik-rur.ogg');
    musicClip.load().then((AudioClip clip) {
      print('music can play: ' + clip.isReadyToPlay.toString());
      print('error: ' + clip.errorString);
      audioManager.music.clip = clip;
      audioManager.music.play();
    });
    document.querySelector('#hull-strength-data').innerHtml =
        player.shieldPower.toString();
    document.querySelector('#speed-data').innerHtml = player.speed.toString();

    if (player is Earth) {
      controls = new EarthControls(player);
    } else if (player is Ship) {
      controls = new ShipControls(player, this);
      earth = new Earth();
      _entities.add(earth);
      player.position.z = 100000.0;
    }
    _entities.add(player);
  }

  Set<Entity> get readOnlyEntitySet {
    Set<Entity> entities = _entities;
    return entities;
  }

  void saveGame();

  void end() {}

  void addEntity(Entity entity) {
    _needsAdded.add(entity);
  }

  void removeEntity(Entity entity) {
    _needsRemoved.add(entity);
  }

  void _start() {
    document.querySelector('#gui').classes.remove('hidden');
    _tick();
  }
  
  void _tick([double elapsed = 0.0]) {
    document.querySelector("#hull-strength-data").innerHtml =
        player.shieldPower.toString();
    document.querySelector("#speed-data").innerHtml = player.speed.toString();
    if (player.target != null) {
      if (player.target.hashCode == earth.hashCode) {
        document.querySelector("#target-name-data").innerHtml = "Earth";
        document.querySelector("#target-hull-strength-data").innerHtml =
            earth.shieldPower.toString();
        int range = player.position.distanceTo(earth.position).round();
        document
            .querySelector("#target-range-data").innerHtml = range.toString();
      }
    }

    _entities.forEach((Entity entity) => entity.update(this, elapsed));
    _entities.addAll(_needsAdded);
    _needsAdded.clear();
    _entities.removeAll(_needsRemoved);
    _needsRemoved.clear();
    renderer.render(elapsed);
    window.requestAnimationFrame(_tick);
  }
}
