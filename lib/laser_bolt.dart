part of alien_war;

class LaserBolt extends Entity {
  static const int DEFAULT_SHIELD_POWER = 10;
  static const double DEFAULT_SPEED = 200.0;
  static const int MAX_RANGE = 100000;
  static const int HIT_RADIUS = 2000;
  static final Projector projector = new Projector();
  static final InputManager input = new InputManager();

  static final Set<LaserBolt> cache = new Set<LaserBolt>();
  static int nextId = 0;

  Entity owner;
  int damage;

  factory LaserBolt(Entity owner, int damage, Game game) {
    LaserBolt bolt;
    if (cache.length > 0) {
      bolt = cache.last;
      cache.remove(cache.last);
    } else {
      bolt = new LaserBolt._(owner, damage);
    }
    bolt.position = owner.position.clone();
    Vector3 mouseVec = new Vector3.zero()
      ..x = input.mouseX
      ..y = input.mouseY;
    Ray picker = projector.pickingRay(mouseVec, game.renderer.camera);
    bolt.attitude = new Attitude()..look = picker.direction;
    return bolt;
  }

  LaserBolt._(Entity owner, this.damage) : super(
          id: ++nextId,
          speed: DEFAULT_SPEED,
          position: owner.position.clone(),
          attitude: owner.attitude.clone(),
          shieldPower: DEFAULT_SHIELD_POWER) {
    this.owner = owner;
  }

  @override
  void update(Game game, double elapsed) {
    position.add(attitude.look.scaled(speed));

    game.readOnlyEntitySet.forEach((Entity entity) {
      if (entity.hashCode == owner.hashCode ||
          entity.hashCode == this.hashCode) {
        return;
      }
      if (position.distanceTo(entity.position) < HIT_RADIUS) {
        //print(entity.hashCode.toString() + ', ' + hashCode.toString());
        //print(game.readOnlyEntitySet.length);
        entity.shieldPower -= damage;
        game.renderer.removeLaser(this);
        game.removeEntity(this);
        cache.add(this);
      }
    });

    if (position.distanceTo(owner.position) > MAX_RANGE) {
      game.renderer.removeLaser(this);
      game.removeEntity(this);
      cache.add(this);
    }
  }
}
