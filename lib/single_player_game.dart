part of alien_war;

class SinglePlayerGame extends Game {
  static const int INIT_NUM_SHIPS = 10;

  SinglePlayerGame(Entity player) : super(player);

  @override
  void saveGame() {}
}
