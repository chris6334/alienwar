part of alien_war;

/**
 * Attitude class.
 *
 * Rotations done well.
 */

class Attitude {
  static final Quaternion rotator = new Quaternion.identity();
  Quaternion orientation;
  Vector3 cross;
  Vector3 up;
  Vector3 look;

  Attitude({this.cross, this.up, this.look}) {
    orientation = new Quaternion.identity();
    if (cross == null) {
      cross = new Vector3.zero()..x = 1.0;
    }
    if (up == null) {
      up = new Vector3.zero()..y = 1.0;
    }
    if (look == null) {
      look = new Vector3.zero()..z = 1.0;
    }
  }

  operator [](int index) {
    int i = index ~/ 3;
    int m = i % 3;
    return (i == 0) ? cross[m] : (i == 1) ? up[m] : look[m];
  }

  operator []=(int index, double value) {
    int i = index ~/ 3;
    int m = i % 3;
    if (i == 0) {
      cross[m] = value;
    } else if (i == 1) {
      up[m] = value;
    } else {
      look[m] = value;
    }
  }

  Attitude clone() => new Attitude(
      cross: cross.clone(),
      up: up.clone(),
      look: look.clone());

  void pitchUp(double theta) {
    rotator.setAxisAngle(cross, theta);
    rotator.rotate(up);
    rotator.rotate(look);
    rotator.setAxisAngle(cross, -theta);
    orientation.copyFrom(rotator * orientation);
  }

  void pitchDown(double theta) {
    rotator.setAxisAngle(cross, -theta);
    rotator.rotate(up);
    rotator.rotate(look);
    rotator.setAxisAngle(cross, theta);
    orientation.copyFrom(rotator * orientation);
  }

  void yawLeft(double theta) {
    rotator.setAxisAngle(up, -theta);
    rotator.rotate(cross);
    rotator.rotate(look);
    rotator.setAxisAngle(up, theta);
    orientation.copyFrom(rotator * orientation);
  }

  void yawRight(double theta) {
    rotator.setAxisAngle(up, theta);
    rotator.rotate(cross);
    rotator.rotate(look);
    rotator.setAxisAngle(up, -theta);
    orientation.copyFrom(rotator * orientation);
  }

  void rollLeft(double theta) {
    rotator.setAxisAngle(look, -theta);
    rotator.rotate(cross);
    rotator.rotate(up);
    rotator.setAxisAngle(look, theta);
    orientation.copyFrom(rotator * orientation);
  }

  void rollRight(double theta) {
    rotator.setAxisAngle(look, theta);
    rotator.rotate(cross);
    rotator.rotate(up);
    rotator.setAxisAngle(look, -theta);
    orientation.copyFrom(rotator * orientation);
  }

  Matrix3 toMatrix3(Matrix3 matrix) => matrix.setValues(cross[0], cross[1],
      cross[2], up[0], up[1], up[2], look[0], look[1], look[2]);
}
