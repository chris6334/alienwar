part of alien_war;

class EarthControls extends Controls{
  static EarthControls _singleton;
  Earth _player;
  InputManager input;

  factory EarthControls(Earth player) {
    if(_singleton == null) {
      _singleton = new EarthControls._(player);
    }
    return _singleton;
  }

  EarthControls._(this._player) {
    input = new InputManager();
    input.registerKeyListener(yokeControl);
    input.registerMouseButtonListener(laserTrigger);
  }

  get player => _player;
  set player(Earth player) => _player = player;

  void update(){}

  void yokeControl(Set<int> pressedKeys) {
    if (pressedKeys.contains(input.keys['W_Key'])) {
      _player.pitchDown();
    } else if (pressedKeys.contains(input.keys['S_KEY'])) {
      _player.pitchUp();
    } else if (pressedKeys.contains(input.keys['A_KEY'])) {
      _player.yawLeft();
    } else if (pressedKeys.contains(input.keys['D_KEY'])) {
      _player.yawRight();
    }
  }

  void laserTrigger(bool mouseZeroDown, bool _1, bool _2) {
    if (mouseZeroDown) {
      _player.state.add(EarthState.Firing);
    } else {
      _player.state.remove(EarthState.Firing);
    }
  }
}
