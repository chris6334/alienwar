part of alien_war;

enum ShipState {
  PitchingUp,
  PitchingDown,
  YawingLeft,
  YawingRight,
  RollingRight,
  RollingLeft,
  Accelerating,
  Deccelerating,
  Firing,
}

class Ship extends Entity {
  static const int FIRE_RATE_MILLISECONDS = 300;
  Set<ShipState> state;
  int firePower;
  int fireCooldown = 0;

  Ship({this.firePower: 10, int shieldPower: 100, int id: -1, Vector3 position,
      double speed: 0.0, double rotationSpeed: PI / 100.0, Attitude attitude})
      : super(
          id: id,
          position: position,
          speed: speed,
          rotationSpeed: rotationSpeed,
          attitude: attitude,
          shieldPower: shieldPower) {
    state = new Set<ShipState>();
  }

  @override
  void update(Game game, double elapsed) {
    if (state.contains(ShipState.PitchingUp)) {
      attitude.pitchUp(rotationSpeed);
    }
    if (state.contains(ShipState.PitchingDown)) {
      attitude.pitchDown(rotationSpeed);
    }
    if (state.contains(ShipState.YawingLeft)) {
      attitude.yawLeft(rotationSpeed);
    }
    if (state.contains(ShipState.YawingRight)) {
      attitude.yawRight(rotationSpeed);
    }
    if (state.contains(ShipState.RollingLeft)) {
      attitude.rollLeft(rotationSpeed);
    }
    if (state.contains(ShipState.RollingRight)) {
      attitude.rollRight(rotationSpeed);
    }

    if (state.contains(ShipState.Firing)) {
      if (fireCooldown < elapsed.toInt()) {
        LaserBolt bolt = new LaserBolt(this, firePower, game);
        game.addEntity(bolt);
        game.renderer.addLaser(bolt);
        fireCooldown = elapsed.toInt() + FIRE_RATE_MILLISECONDS;
      }
    }

    this.position.add(attitude.look.scaled(speed));
  }
}
