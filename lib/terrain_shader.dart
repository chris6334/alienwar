part of alien_war;

var TerrainShader = {
  'uniforms': {'tDisplacement': new Uniform('t', null), 'elapsed': new Uniform('f', 0.0)},
  'vertexShader': """  
    
    uniform sampler2D tDisplacement;
    uniform float elapsed;
    varying float vHeight;
    void main() {
      float height = texture2D(tDisplacement, (uv + 0.5) / 2.0).x;
      vHeight = height;
      vec4 pos = vec4(position.x, position.y, position.z - height * 15000.0, 1.0);
      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos);
    }

   """,
  'fragmentShader': """

    varying float vHeight;
    void main() {
      gl_FragColor = vec4(0.0, 1.0 - vHeight, 0.0, 1.0);
    }

   """
};
