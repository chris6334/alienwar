part of alien_war;

typedef void rendererReadyCallback();

class Renderer {
  static const int NUM_ASYNC_LOADS = 6;
  static const double TILE_SIZE = 1.0e5;
  static const int GRID_RESOLUTION = 256;
  static const double NEAR_CLIP = 1.0;
  static const double FAR_CLIP = 2.0e5;

  Game game;
  WebGLRenderer renderer;
  PerspectiveCamera camera;
  Scene scene;
  Object3D shipModel;
  Object3D earthModel;
  Object3D terrain;
  Geometry lineGeometry;
  LineBasicMaterial lineMaterial;
  int loadedCount = 0;
  rendererReadyCallback callback;

  Renderer(this.game, this.callback) {
    renderer = new WebGLRenderer();
    document.body.append(renderer.domElement);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(new Color(0x000010), 1.0);
    camera = new PerspectiveCamera(
        35.0, window.innerWidth / window.innerHeight, NEAR_CLIP, FAR_CLIP);
    camera.position.z = 100.0;
    window.onResize.listen(onWindowResize);
    scene = new Scene();
    var light = new AmbientLight(0xffffff);
    scene.add(light);

    Random rand = new Random();
    Geometry particles = new Geometry();
    ParticleBasicMaterial material =
        new ParticleBasicMaterial(color: 0xFFFFFF, size: 1);
    for (int i = 0; i < 10000; ++i) {
      double x = rand.nextDouble() * 400000 - 200000;
      double y = rand.nextDouble() * 400000 - 200000;
      double z = rand.nextDouble() * 400000 - 200000;
      Vector3 particle = new Vector3(x, y, z);
      particles.vertices.add(particle);
    }
    ParticleSystem particleSystem = new ParticleSystem(particles, material);
    scene.add(particleSystem);
    
    OBJLoader objLoader = new OBJLoader(useMtl: false);
    objLoader.load('models/superiority_fighter5.obj').then((Object3D object) {
      ImageUtils.loadTexture('images/scratched_surface_texture4.png',
          onLoad: (Texture texture) {
        shipModel = new Mesh(object.children[0].children[1].geometry,
            new MeshBasicMaterial(map: texture));
        shipModel.useQuaternion = true;
        shipModel.quaternion = game.player.attitude.orientation;
        shipModel.position = game.player.position;

        camera.up = game.player.attitude.up;

        scene.add(shipModel);
        _loaded();
      });
    });

    objLoader.load('models/turret.obj').then((Object3D object) {
      ImageUtils.loadTexture('images/scratched_surface_texture3.png',
          onLoad: (Texture texture) {
        earthModel = new Mesh(object.children[0].children[1].geometry,
            new MeshBasicMaterial(map: texture));
        earthModel.position = game.earth.position;
        earthModel.position.setValues(0.0, -7000.0, 0.0);
        earthModel.scale.setValues(500.0, 500.0, 500.0);
        scene.add(earthModel);
        _loaded();
      });
    });

    terrain = new Object3D();
    scene.add(terrain);

    PlaneGeometry geometry = new PlaneGeometry(
        TILE_SIZE, TILE_SIZE, GRID_RESOLUTION, GRID_RESOLUTION);
    geometry.computeFaceNormals();
    geometry.computeVertexNormals();

    ImageUtils.loadTexture('images/terrain.png', onLoad: (Texture heightMap) {
      Map<String, Uniform> uniforms =
          UniformsUtils.clone(TerrainShader['uniforms']);
      uniforms['tDisplacement'].value = heightMap;
      ShaderMaterial material = new ShaderMaterial(
          uniforms: uniforms,
          vertexShader: TerrainShader['vertexShader'],
          fragmentShader: TerrainShader['fragmentShader'],
          lights: false,
          fog: false);

      Mesh tile = new Mesh(geometry, material);
      tile.position.setValues(-TILE_SIZE / 2, 0.0, -TILE_SIZE / 2);
      tile.rotation.x = -PI / 2;
      terrain.add(tile);
      _loaded();
    }, onError: (String msg) => print(msg));

    lineGeometry = new Geometry();
    lineMaterial = new LineBasicMaterial(linewidth: 20);
    lineGeometry.vertices.add(new Vector3.zero());
    lineGeometry.vertices.add(new Vector3.zero()..z = -200.0);

    ImageUtils.loadTexture('images/terrain-flip-x.png',
        onLoad: (Texture heightMap) {
      Map<String, Uniform> uniforms =
          UniformsUtils.clone(TerrainShader['uniforms']);
      uniforms['tDisplacement'].value = heightMap;
      ShaderMaterial material = new ShaderMaterial(
          uniforms: uniforms,
          vertexShader: TerrainShader['vertexShader'],
          fragmentShader: TerrainShader['fragmentShader'],
          lights: false,
          fog: false);

      Mesh tile = new Mesh(geometry, material);
      tile.position.setValues(-TILE_SIZE / 2, 0.0, TILE_SIZE / 2);
      tile.rotation.x = -PI / 2;
      terrain.add(tile);
      _loaded();
    }, onError: (String msg) => print(msg));

    ImageUtils.loadTexture('images/terrain-flip-xy.png',
        onLoad: (Texture heightMap) {
      Map<String, Uniform> uniforms =
          UniformsUtils.clone(TerrainShader['uniforms']);
      uniforms['tDisplacement'].value = heightMap;
      ShaderMaterial material = new ShaderMaterial(
          uniforms: uniforms,
          vertexShader: TerrainShader['vertexShader'],
          fragmentShader: TerrainShader['fragmentShader'],
          lights: false,
          fog: false);

      Mesh tile = new Mesh(geometry, material);
      tile.position.setValues(TILE_SIZE / 2, 0.0, TILE_SIZE / 2);
      tile.rotation.x = -PI / 2;
      terrain.add(tile);
      _loaded();
    }, onError: (String msg) => print(msg));

    ImageUtils.loadTexture('images/terrain-flip-y.png',
        onLoad: (Texture heightMap) {
      Map<String, Uniform> uniforms =
          UniformsUtils.clone(TerrainShader['uniforms']);
      uniforms['tDisplacement'].value = heightMap;
      ShaderMaterial material = new ShaderMaterial(
          uniforms: uniforms,
          vertexShader: TerrainShader['vertexShader'],
          fragmentShader: TerrainShader['fragmentShader'],
          lights: false,
          fog: false);

      Mesh tile = new Mesh(geometry, material);
      tile.position.setValues(TILE_SIZE / 2, 0.0, -TILE_SIZE / 2);
      tile.rotation.x = -PI / 2;
      terrain.add(tile);
      _loaded();
    }, onError: (String msg) => print(msg));
  }

  void _loaded() {
    if (++loadedCount == NUM_ASYNC_LOADS) {
      callback();
    }
  }

  void onWindowResize(event) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  void addLaser(LaserBolt bolt) {
    Geometry lineGeometry = new Geometry();
    lineGeometry.vertices.add(new Vector3.zero());
    lineGeometry.vertices.add(bolt.attitude.look.scaled(100.0));
    Line line = new Line(lineGeometry, lineMaterial);
    line.position = bolt.position;
    scene.add(line);
    scene.children.toString();
  }

  void removeLaser(LaserBolt bolt) {
    Object3D object;
    scene.children.forEach((Object3D obj) {
      if (obj.position.hashCode == bolt.position.hashCode) {
        object = obj;
      }
    });
    scene.remove(object);
  }

  void render(double elapsed) {
    terrain.children.forEach((Mesh child) {
      (child.material as ShaderMaterial).uniforms['elapsed'].value = elapsed;
    });

    camera.position.setFrom(game.player.position);
    camera.position.add(game.player.attitude.look.scaled(-70.0));
    camera.position.add(game.player.attitude.up.scaled(20.0));
    camera.lookAt(game.player.position);
    camera.position.add(game.player.attitude.up.scaled(5.0));
    renderer.render(scene, camera);
  }
}
