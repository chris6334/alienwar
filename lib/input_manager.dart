part of alien_war;

/**
 * InputManager class
 */

typedef keyListener(Set<int> pressedKeys);
typedef mouseButtonListener(
    bool mouseZeroDown, bool mouseOneDown, bool mouseTwoDown);
typedef mouseMoveListener(double mouseX, double mouseY);
typedef mouseWheelListener(bool trueIfUpFalseIfDown);

class InputManager {
  static InputManager _singleton;

  final List<keyListener> _keyListeners = new List<keyListener>();
  final List<mouseButtonListener> _mouseButtonListeners =
      new List<mouseButtonListener>();
  final List<mouseMoveListener> _mouseMoveListeners =
      new List<mouseMoveListener>();
  final List<mouseWheelListener> _mouseWheelListeners =
      new List<mouseWheelListener>();

  bool mouseZeroDown = false;
  bool mouseOneDown = false;
  bool mouseTwoDown = false;

  double mouseX = 0.0;
  double mouseY = 0.0;

  final Set<int> pressedKeys = new Set<int>();

  final Map<String, int> keys = {
    "UP": 38,
    "DOWN": 40,
    "LEFT": 37,
    "RIGHT": 39,
    "TAB": 9,
    "ENTER": 13,
    "SHIFT": 16,
    "CTRL": 17,
    "ALT": 18,
    "SPACE": 32,
    "ZERO": 48,
    "ONE": 49,
    "TWO": 50,
    "THREE": 51,
    "FOUR": 52,
    "FIVE": 53,
    "SIX": 54,
    "SEVEN": 55,
    "EIGHT": 56,
    "NINE": 57,
    "A_KEY": 65,
    "B_KEY": 66,
    "C_KEY": 67,
    "D_KEY": 68,
    "E_KEY": 69,
    "F_KEY": 70,
    "G_KEY": 71,
    "H_KEY": 72,
    "I_KEY": 73,
    "J_KEY": 74,
    "K_KEY": 75,
    "L_KEY": 76,
    "M_KEY": 77,
    "N_KEY": 78,
    "O_KEY": 79,
    "P_KEY": 80,
    "Q_KEY": 81,
    "R_KEY": 82,
    "S_KEY": 83,
    "T_KEY": 84,
    "U_KEY": 85,
    "V_KEY": 86,
    "W_KEY": 87,
    "X_KEY": 88,
    "Y_KEY": 89,
    "Z_KEY": 90
  };

  factory InputManager() {
    if (_singleton == null) {
      _singleton = new InputManager._();
    }
    return _singleton;
  }

  InputManager._() {
    window.onContextMenu.listen((MouseEvent e) => e.preventDefault());
    window.onKeyDown.listen(handleKeyDown);
    window.onKeyUp.listen(handleKeyUp);
    window.onMouseDown.listen(handleMouseDown);
    window.onMouseUp.listen(handleMouseUp);
    window.onMouseMove.listen(handleMouseMove);
    window.onMouseWheel.listen(handleMouseWheel);
  }

  registerKeyListener(keyListener callback) => _keyListeners.add(callback);

  unregisterKeyListener(keyListener callback) => _keyListeners.remove(callback);

  registerMouseButtonListener(mouseButtonListener callback) =>
      _mouseButtonListeners.add(callback);

  unregisterMouseButtonListener(mouseButtonListener callback) =>
      _mouseButtonListeners.remove(callback);

  registerMouseMoveListener(mouseMoveListener callback) =>
      _mouseMoveListeners.add(callback);

  unregisterMouseMoveListener(mouseMoveListener callback) =>
      _mouseMoveListeners.remove(callback);

  registerMouseWheelListener(mouseWheelListener callback) =>
      _mouseWheelListeners.add(callback);

  unregisterMouseWheelListener(mouseWheelListener callback) =>
      _mouseWheelListeners.remove(callback);

  void handleKeyDown(KeyboardEvent event) {
    pressedKeys.add(event.keyCode);
    _keyListeners.forEach((keyListener listener) => listener(pressedKeys));
  }

  void handleKeyUp(KeyboardEvent e) {
    pressedKeys.remove(e.keyCode);
    _keyListeners.forEach((keyListener listener) => listener(pressedKeys));
  }

  void handleMouseDown(MouseEvent e) {
    e.preventDefault();
    if (e.button == 0) {
      mouseZeroDown = true;
    } else if (e.button == 1) {
      mouseOneDown = true;
    } else if (e.button == 2) {
      mouseTwoDown = true;
    }
    _mouseButtonListeners.forEach((mouseButtonListener listener) =>
        listener(mouseZeroDown, mouseOneDown, mouseTwoDown));
  }

  void handleMouseUp(MouseEvent e) {
    e.preventDefault();
    if (e.button == 0) {
      mouseZeroDown = false;
    } else if (e.button == 1) {
      mouseOneDown = false;
    } else if (e.button == 2) {
      mouseTwoDown = false;
    }
    _mouseButtonListeners.forEach((mouseButtonListener listener) =>
        listener(mouseZeroDown, mouseOneDown, mouseTwoDown));
  }

  void handleMouseMove(MouseEvent e) {
    mouseX = 2.0 * e.offset.x / window.innerWidth - 1.0;
    mouseY = -(2.0 * e.offset.y / window.innerHeight - 1.0);
    _mouseMoveListeners
        .forEach((mouseMoveListener listener) => listener(mouseX, mouseY));
  }

  void handleMouseWheel(WheelEvent e) {
    _mouseWheelListeners.forEach((mouseWheelListener listener) => listener(e.deltaY > 0));
  }
}
