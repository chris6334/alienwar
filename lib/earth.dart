part of alien_war;

enum EarthState {
  PitchingUp,
  PitchingDown,
  YawingLeft,
  YawingRight,
  Firing
}

class Earth extends Entity {
  int groundLaserPower;
  int spaceLaserPower;
  Set<EarthState> state = new Set<EarthState>();

  Earth({this.groundLaserPower: 100, this.spaceLaserPower: 100,
      int shieldPower: 100, int id: -1, Vector3 position, double speed: 0.0,
      double rotationSpeed: 4.0, Attitude attitude})
      : super(id: id, position: position, speed: speed, attitude: attitude, shieldPower: shieldPower);

  @override
  void update(Game game, double elapsed) {
  }

  void pitchUp() => attitude.pitchUp(rotationSpeed);
  void pitchDown() => attitude.pitchDown(rotationSpeed);
  void yawLeft() => attitude.yawLeft(rotationSpeed);
  void yawRight() => attitude.yawRight(rotationSpeed);
}
