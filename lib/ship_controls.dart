part of alien_war;

class ShipControls extends Controls {
  static ShipControls _singleton;
  Ship _player;
  InputManager input;
  Projector projector;
  Game game;

  factory ShipControls(Ship player, Game game) {
    if (_singleton == null) {
      _singleton = new ShipControls._(player, game);
    }
    return _singleton;
  }

  ShipControls._(this._player, this.game) {
    projector = new Projector();
    input = new InputManager();
    input.registerKeyListener(yokeControl);
    input.registerMouseButtonListener(laserTrigger);
    input.registerMouseButtonListener(throttleHalt);
    input.registerMouseWheelListener(throttleControl);
    _player.attitude.yawLeft(PI);
  }

  get player => _player;
  set player(Ship player) => _player = player;

  void throttleControl(bool up) {
    if (up) {
      _player.speed -= 10.0;
    } else {
      _player.speed += 10.0;
    }
  }

  void throttleHalt(bool _1, bool mouseOneDown, bool _2) {
    if (mouseOneDown) {
      _player.speed = 0.0;
    }
  }

  void yokeControl(Set<int> pressedKeys) {
    if (pressedKeys.contains(input.keys['W_KEY'])) {
      print('pitch down');
      _player.state.add(ShipState.PitchingDown);
    } else {
      _player.state.remove(ShipState.PitchingDown);
    }

    if (pressedKeys.contains(input.keys['S_KEY'])) {
      print('pitch up');
      _player.state.add(ShipState.PitchingUp);
    } else {
      _player.state.remove(ShipState.PitchingUp);
    }

    if (pressedKeys.contains(input.keys['A_KEY'])) {
      print('yaw left');
      _player.state.add(ShipState.YawingLeft);
    } else {
      _player.state.remove(ShipState.YawingLeft);
    }

    if (pressedKeys.contains(input.keys['D_KEY'])) {
      print('yaw right');
      _player.state.add(ShipState.YawingRight);
    } else {
      _player.state.remove(ShipState.YawingRight);
    }

    if (pressedKeys.contains(input.keys['Q_KEY'])) {
      print('roll left');
      _player.state.add(ShipState.RollingRight);
    } else {
      _player.state.remove(ShipState.RollingRight);
    }

    if (pressedKeys.contains(input.keys['E_KEY'])) {
      print('roll right');
      _player.state.add(ShipState.RollingLeft);
    } else {
      _player.state.remove(ShipState.RollingLeft);
    }
  }

  void laserTrigger(bool mouseZeroDown, bool _1, bool mouseTwoDown) {
    if (mouseTwoDown) {
      _player.state.add(ShipState.Firing);
    } else {
      _player.state.remove(ShipState.Firing);
    }

    if (mouseZeroDown) {
      print(input.mouseX.toString() + ', ' + input.mouseY.toString());
      Ray picker = projector.pickingRay(new Vector3.zero()
        ..x = input.mouseX
        ..y = input.mouseY, game.renderer.camera);
      List<Intersect> hits =
          picker.intersectObjects(game.renderer.scene.children);
      print(hits.length.toString());
      if (hits.length > 0) {
        if(hits[0].object.position.hashCode == game.earth.position.hashCode) {
          game.player.target = game.earth;
        }
      }
    }
  }
}
