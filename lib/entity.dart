part of alien_war;

abstract class Entity {
  
  int id;
  Vector3 position;
  double speed;
  double rotationSpeed;
  Attitude attitude;
  Entity target;
  int shieldPower;
  
  Entity({this.id: -1, this.position, this.speed: 0.0, this.rotationSpeed: 4.0,
      this.attitude, this.shieldPower: 100}) {
    if (position == null) {
      position = new Vector3.zero();
    }
    if (attitude == null) {
      attitude = new Attitude();
    }
  }

  void update(Game game, double elaplsed);
}
