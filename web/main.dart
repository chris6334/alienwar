import 'package:AlienWar/alien_war.dart';

void main() {
  ButtonElement playEarth = document.querySelector('#play-earth-button');
  ButtonElement playShip = document.querySelector('#play-ship-button');

  AudioManager audioManager = new AudioManager();

  AudioClip musicClip = audioManager.makeClip('music', 'audio/star-trek.ogg');
  musicClip.load().then((AudioClip clip) {
    print('music can play: ' + clip.isReadyToPlay.toString());
    print('error: ' + clip.errorString);
    audioManager.music.clip = clip;
    audioManager.music.play();

    playEarth.onClick.listen((_) {
      document.querySelector('#splash-screen').classes.add('hidden');
      audioManager.pauseMusic();
      new SinglePlayerGame(new Earth());
    });

    playShip.onClick.listen((_) {
      document.querySelector('#splash-screen').classes.add('hidden');
      audioManager.pauseMusic();
      new SinglePlayerGame(new Ship());
    });
  });
}
